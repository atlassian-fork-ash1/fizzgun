mitmproxy==2.0.2
requests==2.18.4
pyzmq==16.0.2
pyyaml==3.12
pystache==0.5.4
