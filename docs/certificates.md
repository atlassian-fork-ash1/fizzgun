### Trusting Fizzgun's mitmproxy Certificate Authority

Once your proxy is configured you can start navigating. However you'll get certificate errors when accessing HTTPS
resources (since [mitmproxy](https://mitmproxy.org/) will be forging certificates in order to be able to intercept
requests). In some cases you may add exceptions, however if the server implements HSTS you won't be able to add those
exceptions.

Instead, you can make your browser (or whatever user-agent you are using) trust Fizzgun's mitmproxy CA. Navigate to
`http://fizzgun.it` and follow the instructions there.

This certificate is generated uniquely per user (to avoid other people from being able to intercept your traffic). The
certificate will be generated the first time you run Fizzgun. The generated certificates are saved in a `.mitmproxy/`
directory in your home directory, if you want a new certificate to be generated just delete that entire directory.
