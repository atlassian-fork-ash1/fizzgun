import unittest.mock

from fizzgun.application.dependencies import FileSystem


class MockFileSystem(FileSystem):
    def given_read_file_returns(self, content: str):
        pass

    def given_files_exist(self, *file_names: str):
        pass


def create_mock_file_system() -> MockFileSystem:
    mock_file_system = unittest.mock.create_autospec(spec=FileSystem, instance=True)

    def given_read_file_returns(content: str):
        mock_file_system.read_file.return_value = content

    def given_files_exist(*file_names: str):
        mock_file_system.is_file.side_effect = lambda filename: filename in file_names

    setattr(mock_file_system, MockFileSystem.given_read_file_returns.__name__, given_read_file_returns)
    setattr(mock_file_system, MockFileSystem.given_files_exist.__name__, given_files_exist)

    mock_file_system.given_files_exist()

    return mock_file_system
