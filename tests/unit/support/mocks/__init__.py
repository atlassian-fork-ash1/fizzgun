from tests.unit.support.mocks.mock_id_generator import create_mock_id_generator, MockIdGenerator
from tests.unit.support.mocks.mock_module_importer import create_mock_module_importer, MockModuleImporter
from tests.unit.support.mocks.mock_stdout_writer import create_mock_stdout_writer, MockStdoutWriter
from tests.unit.support.mocks.mock_random_generator import create_mock_random_generator, MockRandomGenerator
from tests.unit.support.mocks.mock_http_client import create_mock_http_client, MockHttpClient
from tests.unit.support.mocks.mock_file_system import create_mock_file_system, MockFileSystem


class MockFactory(object):
    create_mock_id_generator = create_mock_id_generator
    create_mock_module_importer = create_mock_module_importer
    create_mock_stdout_writer = create_mock_stdout_writer
    create_mock_random_generator = create_mock_random_generator
    create_mock_http_client = create_mock_http_client
    create_mock_file_system = create_mock_file_system


__all__ = ['MockFactory', 'MockIdGenerator', 'MockModuleImporter',
           'MockStdoutWriter', 'MockRandomGenerator', 'MockHttpClient', 'MockFileSystem']
