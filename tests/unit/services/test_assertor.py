import pytest

from fizzgun.models.expectation import Expectations
from tests.unit.support.builders.response_builder import ResponseBuilder
from tests.unit.support.builders.scenarios import AssertorScenarioBuilder


def a_non_matching_expectation_and_response():
    expectations = Expectations()
    expectations.expect('body').to.be_equal_to('foo')

    response = ResponseBuilder().with_body('bar')

    return expectations._marshall(), response.build()


def test_should_produce_no_ouput_when_no_expectations_are_set(
    assertor_scenario_builder: AssertorScenarioBuilder
):
    output = (
        assertor_scenario_builder
        .given_message_contains_expectations(Expectations()._marshall())
        .given_message_contains_http_response(ResponseBuilder().build())
        .when_assertor_is_executed()
    )
    assert not output.results


def test_should_produce_no_ouput_when_response_meets_expectations(
    assertor_scenario_builder: AssertorScenarioBuilder
):
    expectations = Expectations()
    expectations.expect('body').to.be_equal_to('test')

    output = (
        assertor_scenario_builder
        .given_message_contains_expectations(expectations._marshall())
        .given_message_contains_http_response(ResponseBuilder().with_body('test').build())
        .when_assertor_is_executed()
    )
    assert not output.results


def test_should_produce_one_ouput_when_response_does_not_meet_a_single_expectation(
    assertor_scenario_builder: AssertorScenarioBuilder
):
    expectations, response = a_non_matching_expectation_and_response()

    output = (
        assertor_scenario_builder
        .given_message_contains_expectations(expectations)
        .given_message_contains_http_response(response)
        .when_assertor_is_executed()
    )
    assert len(output.results) == 1


def test_should_keep_original_properties_in_the_output_message(
    assertor_scenario_builder: AssertorScenarioBuilder
):
    expectations, response = a_non_matching_expectation_and_response()

    output = (
        assertor_scenario_builder
        .given_message_contains_expectations(expectations)
        .given_message_contains_http_response(response)
        .given_message_contains_extra_properties(foo='bar')
        .when_assertor_is_executed()
    )
    assert output.last['response'] == response
    assert output.last['expectations'] == expectations
    assert output.last['foo'] == 'bar'


def test_should_inclued_a_list_of_errors_in_the_output_message(
    assertor_scenario_builder: AssertorScenarioBuilder
):
    expectations, response = a_non_matching_expectation_and_response()

    output = (
        assertor_scenario_builder
        .given_message_contains_expectations(expectations)
        .given_message_contains_http_response(response)
        .when_assertor_is_executed()
    )
    assert output.last['errors']


def test_should_produce_one_output_when_at_least_one_expectation_does_not_match(
    assertor_scenario_builder: AssertorScenarioBuilder
):
    expectations = Expectations()
    expectations.expect('status').to.be_equal_to(200)
    expectations.expect('status').to.be_equal_to(203)
    expectations.expect('status').to.be_in(200, 201, 202)

    response = ResponseBuilder().with_status(200)

    output = (
        assertor_scenario_builder
        .given_message_contains_expectations(expectations._marshall())
        .given_message_contains_http_response(response.build())
        .when_assertor_is_executed()
    )
    assert len(output.results) == 1
    assert output.last['errors'] == ["Expecting 'status' to be equal to [203] (actual: 200)"]


def test_should_include_an_error_message_for_each_unmatched_expectation(
    assertor_scenario_builder: AssertorScenarioBuilder
):
    expectations = Expectations()
    expectations.expect('status').to.be_equal_to(201)
    expectations.expect('status').to.be_equal_to(202)
    expectations.expect('status').to.be_equal_to(203)

    response = ResponseBuilder().with_status(202)

    output = (
        assertor_scenario_builder
        .given_message_contains_expectations(expectations._marshall())
        .given_message_contains_http_response(response.build())
        .when_assertor_is_executed()
    )
    assert len(output.results) == 1
    assert output.last['errors'] == [
        "Expecting 'status' to be equal to [201] (actual: 202)",
        "Expecting 'status' to be equal to [203] (actual: 202)"
    ]


def test_should_process_all_incoming_messages(
    assertor_scenario_builder: AssertorScenarioBuilder
):
    output = (
        assertor_scenario_builder
        .given_incoming_messages(
            a_non_matching_expectation_and_response(),
            a_non_matching_expectation_and_response(),
            a_non_matching_expectation_and_response())
        .when_assertor_is_executed()
    )

    assert len(output.results) == 3


expectations_scenarios = {
    'equal success':
        (lambda e: e.expect('status').to.be_equal_to(200),
         ResponseBuilder().with_status(200).build(),
         None),

    'not equal success':
        (lambda e: e.expect('status').not_to.be_equal_to(500),
         ResponseBuilder().with_status(200).build(),
         None),

    'equal failure':
        (lambda e: e.expect('status').to.be_equal_to(200),
         ResponseBuilder().with_status(500).build(),
         "Expecting 'status' to be equal to [200] (actual: 500)"),

    'not equal failure':
        (lambda e: e.expect('status').not_to.be_equal_to(500),
         ResponseBuilder().with_status(500).build(),
         "Expecting 'status' not to be equal to [500] (actual: 500)"),

    'match success':
        (lambda e: e.expect('body').to.match('^[a-z]+$'),
         ResponseBuilder().with_body('alowercaseword').build(),
         None),

    'not match success':
        (lambda e: e.expect('body').not_to.match('^[a-z]+$'),
         ResponseBuilder().with_body('almostLowercase').build(),
         None),

    'match failure':
        (lambda e: e.expect('body').to.match('^[a-z]+$'),
         ResponseBuilder().with_body('almostLowercase').build(),
         "Expecting 'body' to match ['^[a-z]+$'] (actual: almostLowercase)"),

    'not match failure':
        (lambda e: e.expect('body').not_to.match('^[a-z]+$'),
         ResponseBuilder().with_body('alowercaseword').build(),
         "Expecting 'body' not to match ['^[a-z]+$'] (actual: alowercaseword)"),

    'include success':
        (lambda e: e.expect('body').to.include('this', 'that'),
         ResponseBuilder().with_body('with this and that').build(),
         None),

    'not include success':
        (lambda e: e.expect('body').not_to.include('this', 'those'),
         ResponseBuilder().with_body('with this and that').build(),
         None),

    'include failure':
        (lambda e: e.expect('body').to.include('this', 'that'),
         ResponseBuilder().with_body('with this and those').build(),
         "Expecting 'body' to include ['this', 'that'] (actual: with this and those)"),

    'not include failure':
        (lambda e: e.expect('body').not_to.include('this', 'that'),
         ResponseBuilder().with_body('with this and that').build(),
         "Expecting 'body' not to include ['this', 'that'] (actual: with this and that)"),

    'in range success (left)':
        (lambda e: e.expect('status').to.be_in_range(200, 205),
         ResponseBuilder().with_status(200).build(),
         None),

    'in range success (middle)':
        (lambda e: e.expect('status').to.be_in_range(200, 205),
         ResponseBuilder().with_status(202).build(),
         None),

    'in range success (right)':
        (lambda e: e.expect('status').to.be_in_range(200, 205),
         ResponseBuilder().with_status(205).build(),
         None),

    'not in range success (left)':
        (lambda e: e.expect('status').not_to.be_in_range(200, 205),
         ResponseBuilder().with_status(199).build(),
         None),

    'not in range success (right)':
        (lambda e: e.expect('status').not_to.be_in_range(200, 205),
         ResponseBuilder().with_status(206).build(),
         None),

    'in range failure':
        (lambda e: e.expect('status').to.be_in_range(200, 205),
         ResponseBuilder().with_status(500).build(),
         "Expecting 'status' to be in range [200, 205] (actual: 500)"),

    'not in range failure':
        (lambda e: e.expect('status').not_to.be_in_range(200, 205),
         ResponseBuilder().with_status(200).build(),
         "Expecting 'status' not to be in range [200, 205] (actual: 200)"),

    'in success':
        (lambda e: e.expect('body').to.be_in('one', 'two'),
         ResponseBuilder().with_body('two').build(),
         None),

    'not in success':
        (lambda e: e.expect('status').not_to.be_in(401, 403, 404),
         ResponseBuilder().with_status(402).build(),
         None),

    'in failure':
        (lambda e: e.expect('body').to.be_in('one', 'two'),
         ResponseBuilder().with_body('onetwo').build(),
         "Expecting 'body' to be in ['one', 'two'] (actual: onetwo)"),

    'not in failure':
        (lambda e: e.expect('status').not_to.be_in(401, 403),
         ResponseBuilder().with_status(401).build(),
         "Expecting 'status' not to be in [401, 403] (actual: 401)"),

    'in ranges success (range-left)':
        (lambda e: e.expect('status').to.be_in_ranges('200-399,404,510-512'),
         ResponseBuilder().with_status(200).build(),
         None),

    'in ranges success (range-right)':
        (lambda e: e.expect('status').to.be_in_ranges('200-399,404,510-512'),
         ResponseBuilder().with_status(512).build(),
         None),

    'in ranges success (range-middle)':
        (lambda e: e.expect('status').to.be_in_ranges('200-399,404,510-512'),
         ResponseBuilder().with_status(303).build(),
         None),

    'in ranges success (item)':
        (lambda e: e.expect('status').to.be_in_ranges('200-399,404,510-512'),
         ResponseBuilder().with_status(404).build(),
         None),

    'not in ranges success (range-left)':
        (lambda e: e.expect('status').not_to.be_in_ranges('200-399,404,510-512'),
         ResponseBuilder().with_status(199).build(),
         None),

    'not in ranges success (range-right)':
        (lambda e: e.expect('status').not_to.be_in_ranges('200-399,404,510-512'),
         ResponseBuilder().with_status(513).build(),
         None),

    'not in ranges success (other)':
        (lambda e: e.expect('status').not_to.be_in_ranges('200-399,404,510-512'),
         ResponseBuilder().with_status(415).build(),
         None),

    'in ranges failure':
        (lambda e: e.expect('status').to.be_in_ranges('200-399,404,510-512'),
         ResponseBuilder().with_status(403).build(),
         "Expecting 'status' to be in ranges ['200-399,404,510-512'] (actual: 403)"),

    'not in ranges failure':
        (lambda e: e.expect('status').not_to.be_in_ranges('200-399,404,510-512'),
         ResponseBuilder().with_status(215).build(),
         "Expecting 'status' not to be in ranges ['200-399,404,510-512'] (actual: 215)")
}


parametrized_expectation_scenarios = pytest.mark.parametrize(
    'setup_expectations,http_response,expected_error',
    [value for value in expectations_scenarios.values()],
    ids=[key for key in expectations_scenarios.keys()]
)


@parametrized_expectation_scenarios
def test_should_verify_the_expectations_agains_the_response(
    setup_expectations, http_response, expected_error, assertor_scenario_builder: AssertorScenarioBuilder
):
    expectations = Expectations()
    setup_expectations(expectations)

    output = (
        assertor_scenario_builder
        .given_message_contains_expectations(expectations._marshall())
        .given_message_contains_http_response(http_response)
        .when_assertor_is_executed()
    )

    if expected_error:
        assert len(output.results) == 1
        assert output.last['errors'] == [expected_error]
    else:
        assert not output.results
